package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.NAME, using = "submitButton")
	WebElement eleClickCreateLead;

	@FindBy(how = How.ID, using = "createLeadForm_companyName")
	WebElement eleEnterCompanyName;
	@FindBy(how = How.ID, using = "createLeadForm_firstName")
	WebElement eleEnterFirstName;
	@FindBy(how = How.ID, using = "createLeadForm_lastName")
	WebElement eleEnterLastName;

	public CreateLeadPage enterCompanyName(String cname) {
		clearAndType(eleEnterCompanyName, cname);
		return this;
	}

	public CreateLeadPage enterFirstName(String fname) {
		clearAndType(eleEnterFirstName, fname);
		return this;
	}

	public CreateLeadPage enterLastName(String lName) {
		clearAndType(eleEnterLastName, lName);
		return this;
	}

	public ViewLeadPage clickCreateLead() {
		// WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleClickCreateLead);
		return new ViewLeadPage();
	}

}
